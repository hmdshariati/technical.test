<?php

namespace Hotelian\Tests\problems\Messpagethi;

use PHPUnit\Framework\TestCase;

class MesspagethiTest extends TestCase
{

    public function testRaceWrite()
    {
        $fname = 'test.tmp';
        $fstream = fopen($fname, 'wb');
        $lock = flock($fstream, LOCK_EX | LOCK_NB);
        self::assertTrue($lock);
        $start = microtime(true);
        $did_write = Messpagethi::writeToFile($fname, 'test', true, 10);
        self::assertFalse($did_write);
        self::assertTrue(microtime(true)-$start>0.09);
        flock($fstream, LOCK_UN);
        fclose($fstream);
    }
    public function testAppend(){
        $fname = 'test.tmp';
        $err = Messpagethi::writeToFile($fname, 'test', true, 10);
        self::assertFalse($err);
        $err = Messpagethi::writeToFile($fname, 'test', false, 10);
        self::assertFalse($err);
        $content = Messpagethi::readFileWithMutex($fname);
        self::assertEquals('testtest', $content);
    }

    public function testSimpleReadWrite()
    {
        $start = microtime(true);
        $written = 'hello ' . random_int(0, 100);
        Messpagethi::writeToFile('test.tmp', $written, true, 100);
        $content = Messpagethi::readFileWithMutex('test.tmp');
        self::assertEquals(
            expected: $written,
            actual: $content
        );
        self::assertTrue(microtime(true)-$start<0.09);
    }
}
