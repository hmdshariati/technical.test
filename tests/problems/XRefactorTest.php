<?php

namespace Hotelian\Tests\problems\XRefactor;

use PHPUnit\Framework\TestCase;

class XRefactorTest extends TestCase
{

    public function testSolve()
    {
        self::assertEquals(
            expected: 'Hello World',
            actual: XRefactor::solve(XRefactor::ASCIIFY, '\\ \\héllo \\ \\\\  world\\'),
        );
        self::assertEquals(
            expected: 'Hello  world',
            actual: XRefactor::solve(XRefactor::NORMALIZE, 'Héllo  woрלд'),
        );
    }
}
