<?php

declare(strict_types=1);

namespace Hotelian\Tests\problems\iterationStation;

use PHPUnit\Framework\TestCase;
use Hotelian\Tests\internal\iterationStation\BaseSolution;

class IterationStationTest extends TestCase
{
    public function testAllNumbersAreProduced()
    {
        $expected_numbers = [];
        for ($i = 0; $i < 20; $i++) {
            $expected_numbers[] = $i;
        }
        $actual = \Hotelian\Tests\internal\iterationStation\IterationStation::makeArrayOfZeroToNineteen();
        self::assertSame(
            $expected_numbers,
            $actual,
            'Not all numbers are there',
        );
    }

    public function testSolutionInstance()
    {
        self::assertInstanceOf(
            \Iterator::class,
            new IterationStation(),
            'Solution must be an Iterator',
        );
        self::assertInstanceOf(
            BaseSolution::class,
            new IterationStation(),
            'Solution must be an implementation of BaseSolution',
        );
    }
}
