<?php

namespace Hotelian\Tests\problems\SolemnColumns;

use PHPUnit\Framework\TestCase;

class SolemnColumnsTest extends TestCase
{

    public function testFillColumns(): void
    {
        $losses = 0;
        $wins = 0;
        $avg = 0;
        $total_rounds = 1000;
        for ($rounds = 0; $rounds < $total_rounds; $rounds++) {
            $weights = [];
            $facts = \Hotelian\Tests\internal\SolemnColumns\SolemnColumns::fetch();
            $row_len = 24;
            $sorted = \Hotelian\Tests\problems\SolemnColumns\SolemnColumns::fillColumns($facts, 3, $row_len);
            foreach ($sorted as $idx => $column) {
                $weights[$idx] = 2;
                foreach ($column as $group) {
                    foreach ($group as $fact) {
                        $weights[$idx] += ceil(strlen($fact->label) / $row_len);
                    }
                }
            }
            $rate = max($weights) / min($weights);
            if ($rate < 1.3) {
                $wins++;
            } else {
                $losses++;
            }
            $avg += $rate / $total_rounds;
        }
        $msg = "Total rounds: $rounds, Wins: $wins, Losses: $losses, Average score: $avg";
        self::assertGreaterThan(0.7, $wins / $total_rounds, $msg);
        self::assertLessThan(1.3, $avg, $msg);
    }
}
