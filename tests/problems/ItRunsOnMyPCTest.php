<?php

namespace Hotelian\Tests\problems\ItRunsOnMyPC;

use PHPUnit\Framework\TestCase;

class ItRunsOnMyPCTest extends TestCase
{

    public function testRun(): void{
        $hwnd  = new ItRunsOnMyPC();
        self::assertNotNull($hwnd->fixMe('Zürich'));
        self::assertStringContainsString('Zürich', $hwnd->fixMe('zürich'));
        self::assertNull($hwnd->fixMe('Germany'));
    }
}
