<?php

namespace Hotelian\Tests\problems\WildWildLoop;

use PHPUnit\Framework\TestCase;

class WildWildLoopTest extends TestCase
{

    public function testSolve()
    {
        $total_rounds = 10;
        $total_time = 0;
        for($rounds = 0; $rounds < $total_rounds; $rounds++) {
            $start = microtime(true);
            WildWildLoop::solve();
            $total_time += microtime(true)-$start;
        }
        self::assertLessThan(0.25, $total_time/$total_rounds);
    }
}
