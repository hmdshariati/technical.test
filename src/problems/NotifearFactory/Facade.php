<?php

declare(strict_types=1);

namespace Hotelian\Tests\problems\NotifearFactory;

use Hotelian\Tests\internal\NotifearFactory\BaseSolution;

/**
 * @title Problem #7 - NotifearFactory
 *
 * @description
 * Our platform consists of many ways to notify varying types of users; In order to have a base message dispatcher for
 * things from notifying for invoice payment, to account verification, we use a Facade message dispatcher class.
 * This dispatcher along with the User ORM, and our Rate limiter class, handles everything notification-related.
 *
 * Your task is to implement three different types of notification services in our system, by using the
 * appropriate design patterns. Usage of any anti-pattern is highly discouraged since the main objective here
 * is to have the most appropriate design.
 *
 * For this task, we provide you with a single API that consists of 3 different dispatching services.
 * 1) Firebase notification (based on device ID)
 * 2) Email notification
 * 3) Phone notification
 *
 * You must validate and handle everything appropriately. Things like handling the phone-prefix and phone-number
 * concatenating them in a correct format.
 *
 * When ready, execute the API (notification_api.exe) and let it listen. Its documentation is below.
 *
 * @objective
 * Design an appropriate architecture for the problem, and implement the notificationDispatcher in a singular
 * Notify() function with the contracted parameters.
 * Your dispatcher must retry an appropriate amount of times when API errors occur.
 * Your dispatcher must conform to the RateLimiting criteria, not surpassing them.
 * Your dispatcher must update RateLimiting bucket appropriately.
 * You must treat each service separately. They are on the same host, but you should treat them as totally different
 * services in your design.
 *
 * @api-email
 * URL: http://localhost:9090/api/v1/email
 * POST Data:
 *    - string email
 *    - string message
 * Headers:
 *    - `X-API-KEY` self::getAuthAccessToken()
 *
 * @api-firebase
 * URL: http://localhost:9090/api/v1/push-notification
 * POST Data:
 *    - string device_id
 *    - string message
 * Headers:
 *    - `X-API-KEY`: self::getAuthAccessToken()
 *
 * @api-sms
 * URL: http://localhost:9090/api/v1/sms
 * POST Data:
 *    - string phone_number
 *    - string message
 * Headers:
 *    - `X-Signature`: SHA256(self::getAuthKey() . self::getAuthSecret() . time())
 *
 * @note Suggested time to solve: 5 hours
 *
 * @category RateLimiting, Bucket RateLimiting, Factory Methods, Facade, SOLID Principles, Strategy Pattern,
 *           PHP OOP, PHP Interfaces and Contracts
 *
 * @author Hotelian.com LTD; London, UK.
 */
class Facade extends BaseSolution
{

    /**
     * Notify a user of a message.
     * Notifications will be sent appropriately according to user's verification status.
     * If a user have their phone verified, the notification message will be sent to their phone.
     * If a user have their email verified, the notification message will be sent to their email.
     * If a user have their firebase verified, the notification message will be sent to their phone(via firebase).
     *
     * @param int $user_id User's ORM incremental ID
     * @param string $message Text message to be sent. Message might contain field aliases such as:
     * {username}, {first_name}, {last_name} or etc. You have to fill these accordingly.
     *
     * @see fetchUser() to fetch a user's details
     *
     * @return bool True if notification was successful, false otherwise
     */
    public static function notify(int $user_id, string $message): bool
    {
        // TODO: Implement notify() method.
    }
}