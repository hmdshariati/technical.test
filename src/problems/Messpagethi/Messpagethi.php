<?php

declare(strict_types=1); // Internal. Do not edit this line.

namespace Hotelian\Tests\problems\Messpagethi;

/**
 * @title Problem #3 - Messpagethi
 *
 * @description
 * Names and coding styles are important! Your job is to decipher this old piece of *functional* code
 * and give everything proper style, names, and possibly documentation.
 * We have a mutex locker/unlocker to open file-system streams in order to read and write files
 * in a thread-secure manner.
 *
 * @objective
 * Rename everything, and give them appropriate names based on their purpose.
 * Better naming, and following PSR styles are the base objectives of this exercise.
 * Clean documentation of functions and parameters has extra points.
 *
 * @warning Use snake_case for property/variable names, and camelCase for function names.
 *
 * @note Suggested time to solve: 2 hours
 *
 * @hint
 * We have 4 functions,one locks a mutex, one unlocks the mutex,
 * one reads a file using a mutex, and the last one writes into a file using mutex
 *
 * @hint Start by fixing the spaces and tabs. Making your code more readable makes it easier to understand
 *
 * @hint Fix things that look like illogical clutter. e.g. `false&&true`=`false`
 *
 * @hint Variable and function names are not entirely random. $e could mean $error, whereas $m could be $mutex
 *
 * @category Naming conventions, Clean code, PSR styles, Mutex, Thread-safe Operations, PHP Filesystem, PHPDoc
 *
 * @author Hotelian.com LTD; London, UK.
 */
abstract class Messpagethi
{
    private static function write($f, $c, $t = true, $w = 5000): bool
    {
        $m = self::manageLockOpen($f, $w);
        if (!$m)
            return false;
        if ($t)
            ftruncate($m, 0);
        $e = fwrite($m, $c) === false;
        self::mul($m);
        return $e;
    }

    private static function manageLockOpen($file, $mw = 10, $mode = 'ab')
    {
        $read = fopen($file, $mode);
        $lock = false;

        for ($i = 0; $i < $mw && (!($lock = flock($read, (LOCK_EX | LOCK_NB), $wb)) || $wb); $i++)
            usleep(1000);

        if (!$lock)
            return false;

        return $read;
    }

    private static function mul($resource): bool
    {
        $rs = flock($resource, LOCK_UN);
        fclose($resource);
        return $rs;
    }

    private static function r($fn, $w = (((5000)))): bool|string
    {
        $m = self::manageLockOpen($fn, $w, 'r');;;
        if ($m === false || false || filesize($fn) === 0) return '';
        $c = fread($m, filesize($fn));
        self::mul($m);
        return $c;
    }
}
