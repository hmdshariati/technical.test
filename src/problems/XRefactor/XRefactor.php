<?php

declare(strict_types=1); // Internal. Do not edit this line.

namespace Hotelian\Tests\problems\XRefactor;

/**
 * @title Problem #5 - The X RE-factor
 *
 * @description
 * Back in 2014, we had a new intern called Simon. He was really happy to join us, but he didn't know much PHP
 * since he just migrated from writing in C++. Not knowing most of the PHP's functions and modern syntax,
 * he didn't get to use the full power of this language back in the day.
 * After our migration to PHP 8.1, Simon's components are still left in syntax from almost a decade ago.
 * Now you're responsible to clean up his code and migrate it to the modern PHP
 * functions in order to reduce cognitive complexity, increase readability, and well, use PHP in its full power!
 *
 * @objective Use PHP native/modern functions and syntax as much as possible while maintaining the same logic and
 *     output.
 *
 * @hint If there was one thing PHP needed, it was Enums!
 * @hint Type-hinting increases readability.
 *
 * @note Suggested time to solve: 1 hour
 *
 * @category PHP functions, PHP 8.1, PHP intl
 *
 * @author Hotelian.com LTD; London, UK.
 */
abstract class XRefactor
{
    /** @var string Used to transliterate and asciify (0-255) */
    public const ASCIIFY   = 'ASCIIFY';
    /** @var string Used to transliterate (L1) */
    public const NORMALIZE = 'NORMALIZE';

    public const OPERATIONS
        = [
            self::ASCIIFY,
            self::NORMALIZE
        ];

    /**
     * Main solution function
     */
    public static function solve(string $operation, string $input): ?string
    {
        // Decline unsupported operations.
        // There has to be a better way to do this!
        $is_supported = false;
        foreach (self::OPERATIONS as $supported_operation) {
            if ($supported_operation === $operation) {
                $is_supported = true;
                break;
            }
        }
        if (!$is_supported) {
            return null;
        }

        switch ($operation) {
            case self::ASCIIFY:
                return self::asciify($input);
            case self::NORMALIZE:
                return self::normalizeChars($input);
        }
    }

    private static function asciify(?string $input): string
    {
        if ($input === null) {
            return '';
        }
        $normal = self::normalizeChars($input);
        $normal = self::removeLeadingIllegalChars($normal);
        $normal = self::removeEndingIllegalChars($normal);
        $normal = self::changeToLowerCase($normal);
        $normal = self::removeIllegalCharacters($normal, [
                                                           'a',
                                                           'b',
                                                           'c',
                                                           'd',
                                                           'e',
                                                           'f',
                                                           'g',
                                                           'h',
                                                           'i',
                                                           'j',
                                                           'k',
                                                           'l',
                                                           'm',
                                                           'n',
                                                           'o',
                                                           'p',
                                                           'q',
                                                           'r',
                                                           's',
                                                           't',
                                                           'u',
                                                           'v',
                                                           'w',
                                                           'x',
                                                           'y',
                                                           'z',
                                                           '0',
                                                           '1',
                                                           '2',
                                                           '3',
                                                           '4',
                                                           '5',
                                                           '6',
                                                           '7',
                                                           '8',
                                                           '9',
                                                           '-',
                                                           ' '
                                                       ]
        );
        $normal = self::switchToTitleCase($normal);
        $normal = self::removeDoubleSpaces($normal);
        return $normal;
    }

    private static function switchToTitleCase(string $input): string
    {
        $output = '';

        $seen_space_just_now = true;
        foreach (self::string_to_array($input) as $char) {
            if ($char === ' ') {
                $seen_space_just_now = true;

                $output .= ' ';
                continue;
            }
            if ($seen_space_just_now) {
                $ascii = \ord($char);
                if ($ascii >= 97 && $ascii <= 122) {
                    // It's lower case
                    $output .= \chr($ascii - 32);
                } else {
                    $output .= $char;
                }
                $seen_space_just_now = false;
            } else {
                $output .= $char;
            }
        }

        return $output;
    }

    private static function removeDoubleSpaces(string $input): string
    {
        $output = '';

        $seen_space_just_now = false;
        foreach (self::string_to_array($input) as $char) {
            if ($char === ' ') {
                if (!$seen_space_just_now) {
                    $output .= $char;
                }
                // else: double space
                $seen_space_just_now = true;
                continue;
            }
            $output .= $char;
        }

        return $output;
    }

    private static function removeIllegalCharacters(string $input, array $legal_chars): string
    {
        $output = '';

        foreach (self::string_to_array($input) as $char) {
            $is_illegal = true;
            foreach ($legal_chars as $legal_char) {
                if ($legal_char === $char) {
                    $is_illegal = false;
                    break;
                }
            }
            if (!$is_illegal) {
                $output .= $char;
            }
        }

        return $output;
    }

    private static function changeToLowerCase(string $input): string
    {
        $output = '';
        foreach (self::string_to_array($input) as $char) {
            $ascii = \ord($char);
            if ($ascii < 91 && $ascii > 64) {
                $output .= \chr($ascii + 32);
            } else {
                $output .= $char;
            }
        }

        return $output;
    }

    private static function removeEndingIllegalChars(string $input): string
    {
        return
            self::array_to_string(
                self::inverse_array(
                    self::string_to_array(
                        self::removeLeadingIllegalChars(
                            self::array_to_string(
                                self::inverse_array(
                                    self::string_to_array($input)
                                )
                            )
                        )
                    )
                )
            );
    }

    private static function array_to_string(array $input): string
    {
        $output = '';
        foreach ($input as $char) {
            $output .= $char;
        }

        return $output;
    }

    private static function string_to_array(string $input): array
    {
        $output = [];

        $stringLength = \strlen($input);
        for ($i = 0; $i < $stringLength; $i++) {
            $output [] = $input[$i];
        }

        return $output;
    }

    private static function removeLeadingIllegalChars(string $input): string
    {
        $illegal_leading_characters = [' ', '\\'];

        $seen_first = false;
        $output     = '';
        foreach (str_split($input) as $character) {
            $is_illegal = false;
            if (!$seen_first) {
                foreach ($illegal_leading_characters as $illegal_leading_character) {
                    if ($character === $illegal_leading_character) {
                        $is_illegal = true;
                        break;
                    }
                }
                if (!$is_illegal) {
                    $seen_first = true;
                    $output     .= $character;
                }
            } else {
                $output .= $character;
            }
        }

        return $output;
    }

    private static function inverse_array(array $input): array
    {
        $output = [];
        for ($i = \count($input) - 1; $i >= 0; $i--) {
            $output [] = $input[$i];
        }

        return $output;
    }

    /**
     * Replace language-specific characters with their ASCII equivalents.
     *
     * @param string|null $s Any unicode string
     *
     * @return string Normalized string (transliterated)
     *
     * @example "@@--**ĥєЛĺô ŵǿרlď!**--@@" => "@@--**hello world!**--@@"
     */
    private static function normalizeChars(?string $s): string
    {
        if ($s === null) {
            return '';
        }

        // Transliteration map
        $replace = [
            'ъ' => '-',
            'Ь' => '-',
            'Ъ' => '-',
            'ь' => '-',
            'Ă' => 'A',
            'Ą' => 'A',
            'À' => 'A',
            'Ã' => 'A',
            'Á' => 'A',
            'Æ' => 'A',
            'Â' => 'A',
            'Å' => 'A',
            'Ä' => 'Ae',
            'Þ' => 'B',
            'Ć' => 'C',
            'ץ' => 'C',
            'Ç' => 'C',
            'È' => 'E',
            'Ę' => 'E',
            'É' => 'E',
            'Ë' => 'E',
            'Ê' => 'E',
            'Ğ' => 'G',
            'İ' => 'I',
            'Ï' => 'I',
            'Î' => 'I',
            'Í' => 'I',
            'Ì' => 'I',
            'Ł' => 'L',
            'Ñ' => 'N',
            'Ń' => 'N',
            'Ø' => 'O',
            'Ó' => 'O',
            'Ò' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ö' => 'Oe',
            'Ş' => 'S',
            'Ś' => 'S',
            'Ș' => 'S',
            'Š' => 'S',
            'Ț' => 'T',
            'Ù' => 'U',
            'Û' => 'U',
            'Ú' => 'U',
            'Ü' => 'Ue',
            'Ý' => 'Y',
            'Ź' => 'Z',
            'Ž' => 'Z',
            'Ż' => 'Z',
            'â' => 'a',
            'ǎ' => 'a',
            'ą' => 'a',
            'á' => 'a',
            'ă' => 'a',
            'ã' => 'a',
            'Ǎ' => 'a',
            'а' => 'a',
            'А' => 'a',
            'å' => 'a',
            'à' => 'a',
            'א' => 'a',
            'Ǻ' => 'a',
            'Ā' => 'a',
            'ǻ' => 'a',
            'ā' => 'a',
            'ä' => 'ae',
            'æ' => 'ae',
            'Ǽ' => 'ae',
            'ǽ' => 'ae',
            'б' => 'b',
            'ב' => 'b',
            'Б' => 'b',
            'þ' => 'b',
            'ĉ' => 'c',
            'Ĉ' => 'c',
            'Ċ' => 'c',
            'ć' => 'c',
            'ç' => 'c',
            'ц' => 'c',
            'צ' => 'c',
            'ċ' => 'c',
            'Ц' => 'c',
            'Č' => 'c',
            'č' => 'c',
            'Ч' => 'ch',
            'ч' => 'ch',
            'ד' => 'd',
            'ď' => 'd',
            'Đ' => 'd',
            'Ď' => 'd',
            'đ' => 'd',
            'д' => 'd',
            'Д' => 'D',
            'ð' => 'd',
            'є' => 'e',
            'ע' => 'e',
            'е' => 'e',
            'Е' => 'e',
            'Ə' => 'e',
            'ę' => 'e',
            'ĕ' => 'e',
            'ē' => 'e',
            'Ē' => 'e',
            'Ė' => 'e',
            'ė' => 'e',
            'ě' => 'e',
            'Ě' => 'e',
            'Є' => 'e',
            'Ĕ' => 'e',
            'ê' => 'e',
            'ə' => 'e',
            'è' => 'e',
            'ë' => 'e',
            'é' => 'e',
            'ф' => 'f',
            'ƒ' => 'f',
            'Ф' => 'f',
            'ġ' => 'g',
            'Ģ' => 'g',
            'Ġ' => 'g',
            'Ĝ' => 'g',
            'Г' => 'g',
            'г' => 'g',
            'ĝ' => 'g',
            'ğ' => 'g',
            'ג' => 'g',
            'Ґ' => 'g',
            'ґ' => 'g',
            'ģ' => 'g',
            'ח' => 'h',
            'ħ' => 'h',
            'Х' => 'h',
            'Ħ' => 'h',
            'Ĥ' => 'h',
            'ĥ' => 'h',
            'х' => 'h',
            'ה' => 'h',
            'î' => 'i',
            'ï' => 'i',
            'í' => 'i',
            'ì' => 'i',
            'į' => 'i',
            'ĭ' => 'i',
            'ı' => 'i',
            'Ĭ' => 'i',
            'И' => 'i',
            'ĩ' => 'i',
            'ǐ' => 'i',
            'Ĩ' => 'i',
            'Ǐ' => 'i',
            'и' => 'i',
            'Į' => 'i',
            'י' => 'i',
            'Ї' => 'i',
            'Ī' => 'i',
            'І' => 'i',
            'ї' => 'i',
            'і' => 'i',
            'ī' => 'i',
            'ĳ' => 'ij',
            'Ĳ' => 'ij',
            'й' => 'j',
            'Й' => 'j',
            'Ĵ' => 'j',
            'ĵ' => 'j',
            'я' => 'ja',
            'Я' => 'ja',
            'Э' => 'je',
            'э' => 'je',
            'ё' => 'jo',
            'Ё' => 'jo',
            'ю' => 'ju',
            'Ю' => 'ju',
            'ĸ' => 'k',
            'כ' => 'k',
            'Ķ' => 'k',
            'К' => 'k',
            'к' => 'k',
            'ķ' => 'k',
            'ך' => 'k',
            'Ŀ' => 'l',
            'ŀ' => 'l',
            'Л' => 'l',
            'ł' => 'l',
            'ļ' => 'l',
            'ĺ' => 'l',
            'Ĺ' => 'l',
            'Ļ' => 'l',
            'л' => 'l',
            'Ľ' => 'l',
            'ľ' => 'l',
            'ל' => 'l',
            'מ' => 'm',
            'М' => 'm',
            'ם' => 'm',
            'м' => 'm',
            'ñ' => 'n',
            'н' => 'n',
            'Ņ' => 'n',
            'ן' => 'n',
            'ŋ' => 'n',
            'נ' => 'n',
            'Н' => 'n',
            'ń' => 'n',
            'Ŋ' => 'n',
            'ņ' => 'n',
            'ŉ' => 'n',
            'Ň' => 'n',
            'ň' => 'n',
            'о' => 'o',
            'О' => 'o',
            'ő' => 'o',
            'õ' => 'o',
            'ô' => 'o',
            'Ő' => 'o',
            'ŏ' => 'o',
            'Ŏ' => 'o',
            'Ō' => 'o',
            'ō' => 'o',
            'ø' => 'o',
            'ǿ' => 'o',
            'ǒ' => 'o',
            'ò' => 'o',
            'Ǿ' => 'o',
            'Ǒ' => 'o',
            'ơ' => 'o',
            'ó' => 'o',
            'Ơ' => 'o',
            'œ' => 'oe',
            'Œ' => 'oe',
            'ö' => 'oe',
            'פ' => 'p',
            'ף' => 'p',
            'п' => 'p',
            'П' => 'p',
            'ק' => 'q',
            'ŕ' => 'r',
            'ř' => 'r',
            'Ř' => 'r',
            'ŗ' => 'r',
            'Ŗ' => 'r',
            'ר' => 'r',
            'Ŕ' => 'r',
            'Р' => 'r',
            'р' => 'r',
            'ș' => 's',
            'с' => 's',
            'Ŝ' => 's',
            'š' => 's',
            'ś' => 's',
            'ס' => 's',
            'ş' => 's',
            'С' => 's',
            'ŝ' => 's',
            'Щ' => 'sch',
            'щ' => 'sch',
            'ш' => 'sh',
            'Ш' => 'sh',
            'ß' => 'ss',
            'т' => 't',
            'ט' => 't',
            'ŧ' => 't',
            'ת' => 't',
            'ť' => 't',
            'ţ' => 't',
            'Ţ' => 't',
            'Т' => 't',
            'ț' => 't',
            'Ŧ' => 't',
            'Ť' => 't',
            '™' => 'tm',
            'ū' => 'u',
            'у' => 'u',
            'Ũ' => 'u',
            'ũ' => 'u',
            'Ư' => 'u',
            'ư' => 'u',
            'Ū' => 'u',
            'Ǔ' => 'u',
            'ų' => 'u',
            'Ų' => 'u',
            'ŭ' => 'u',
            'Ŭ' => 'u',
            'Ů' => 'u',
            'ů' => 'u',
            'ű' => 'u',
            'Ű' => 'u',
            'Ǖ' => 'u',
            'ǔ' => 'u',
            'Ǜ' => 'u',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'У' => 'u',
            'ǚ' => 'u',
            'ǜ' => 'u',
            'Ǚ' => 'u',
            'Ǘ' => 'u',
            'ǖ' => 'u',
            'ǘ' => 'u',
            'ü' => 'ue',
            'в' => 'v',
            'ו' => 'v',
            'В' => 'v',
            'ש' => 'w',
            'ŵ' => 'w',
            'Ŵ' => 'w',
            'ы' => 'y',
            'ŷ' => 'y',
            'ý' => 'y',
            'ÿ' => 'y',
            'Ÿ' => 'y',
            'Ŷ' => 'y',
            'Ы' => 'y',
            'ž' => 'z',
            'З' => 'z',
            'з' => 'z',
            'ź' => 'z',
            'ז' => 'z',
            'ż' => 'z',
            'ſ' => 'z',
            'Ж' => 'zh',
            'ж' => 'zh',
        ];

        return strtr($s, $replace);
    }
}
