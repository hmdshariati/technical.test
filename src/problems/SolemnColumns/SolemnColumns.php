<?php
/** @noinspection PhpDocMissingThrowsInspection */
/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1); // Internal. Do not edit this line.

namespace Hotelian\Tests\problems\SolemnColumns;

use Hotelian\Tests\internal\SolemnColumns\Facility;
use Hotelian\Tests\internal\SolemnColumns\FacilityGroup;
use Hotelian\Tests\internal\SolemnColumns\Solution;

/**
 * @title Problem #2 - Solemn Columns
 *
 * @description
 * Every hotel has a list of facilities that they would provide to their guests; Things like free-wifi, gym, pool, etc.
 * These facilities each go under an umbrella factory. Things like `Hand-sanitizer` go under the `Health` category,
 * while a `Game Room` goes under the category of `Entertainment`.
 * Unfortunately, listing these facilities under their according group makes our UI a mess! One column being very long,
 * while the other two are very short (in height).
 * Your job is to organize these facilities (or rather the groups of facilities) in a way that you shape 3 almost
 * even columns. Return the 3 columns, and their facility groups(which each have an array of facilities).
 * Note that each group takes another 2 lines of space for its own header.
 *
 * @objective
 * Front-end needs 3(dynamic) columns of facility info, with (almost) equally long sizes vertically.
 * Usage of functional programming is encouraged.
 *
 * @note Suggested time to solve: 2 hours
 * @note Each group's weight would be the total of its items weight plus 2 lines for the group name's header
 *
 * @category Dynamic Programming, Recursion, Split flow systems
 *
 * @see FacilityGroup
 * @see Facility
 *
 * @author Hotelian.com LTD; London, UK.
 */
abstract class SolemnColumns implements Solution
{
    /**
     * Main solution implemented by the initiate.
     *
     * @param array<string, Facility> $items Groups of facilities.
     * @param int $column_count Number of columns to divide the $items into
     *
     * @note Items in each group belong together, they may not be separated under any circumstances
     * @note You may not split one category into two columns
     *
     * @return array<array<FacilityGroup>> First dimension has $column_count indices
     */
    public static function fillColumns(array $items, int $column_count = 3, int $column_length = 24): array
    {
        /* #################### Start from here #################### */
        /* You may edit, remove, or add code freely from here on out */
        /* ####################       GO!       #################### */
        return array_chunk($items, (int) (\count($items)/3));
    }
}
