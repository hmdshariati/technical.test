<?php
/** @noinspection PhpDocMissingThrowsInspection */
/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1); // Internal. Do not edit this line.

namespace Hotelian\Tests\problems\WildWildLoop;

use DivineOmega\CliProgressBar\ProgressBar;
use Hotelian\tests\internal\WildWildLoop\Currency;
use Hotelian\Tests\internal\WildWildLoop\Pricing;
use Hotelian\Tests\internal\WildWildLoop\Solution;
use Hotelian\Tests\internal\WildWildLoop\WildWildLoop as Database;

/**
 * @title Problem #1 - The Wild Wild Loop
 *
 * @description
 * Our firm cannot upgrade the systems anymore... Scripts are taking far too long to run, and
 * we can't provide our customers with a smooth silky experience anymore.
 * Our project manager believes there are improvements to be done in our PHP codebase. It's time to reduce our overhead!
 * Technical team refers you to an old component responsible for exchanging prices to the user's currency and then
 * sorting them. There's not much to refactor in it, but one particular loop smells really horrid.
 * Try to reduce the **execution** time of this loop by it more performant using all your skills, without touching
 * its logic, or other classes.
 *
 * @objective
 * Reduce the complexity and therefore the execution time by at least 99.98%(overall under 1s),
 * while maintaining the same logic and output. Even, minor optimizations are intended; Don't overlook anything!
 *
 * @note Suggested time to solve: 1 hour
 *
 * @hint Don't repeat yourself. Loops don't always repeat themselves.
 * @hint Exchange rates don't change in one iteration
 * @hint Type-hinting is a savior!
 * @hint Counts are constant (usually!)
 * @hint IO Operations(echo, var_dump, etc.) are usually the bottleneck. Reduce them!
 * @hint Focus on how the sorting is done. Is there a simpler way to do it?
 * @hint Read the docs! How is that progress-bar working? Are we using it as intended?
 * @hint Database operations are often costly. Cache them whenever possible!
 *
 * @category The Big O Notation, Loop Optimizations, Functional Programming, KISS Principle
 *
 * @author Hotelian.com LTD; London, UK.
 */
abstract class WildWildLoop implements Solution
{
    /**
     * Main solution function
     *
     * @return array<Pricing>
     */
    public static function solve(): array
    {
        $prices = Database::fetch();
        /* #################### Start from here #################### */
        /* You may edit, remove, or add code freely from here on out */
        /* ####################       GO!       #################### */

        // Exchange the prices to USD (user's currency) and return the sorted prices (in USD)
        $progressBar = new ProgressBar();
        $progressBar->setMaxProgress(count($prices));

        for ($i=0; $i<count($prices); $i++) {
//            $prices = Database::fetch();//@todo: why?
            $prices[$i]->cost = $prices[$i]->cost * $prices[$i]->currency->exchange(Currency::USD);
            $prices[$i]->discount = $prices[$i]->discount * $prices[$i]->currency->exchange(Currency::USD);
            $prices[$i]->currency = Currency::USD;

            $progressBar->advance()->display();
            $progressBar->setBarWidth(50)->display();
            $progressBar->setMessage('Working...')->display();
        }

        usort($prices, static function ($a, $b){
            return $a->cost- $a->discount > $b->cost - $b->discount;
        });

        $progressBar->complete();

        return $prices;
    }
}
