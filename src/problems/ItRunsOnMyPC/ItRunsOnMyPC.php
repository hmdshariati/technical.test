<?php
/** @noinspection PhpDocMissingThrowsInspection */
/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1); // Internal. Do not edit this line.

namespace Hotelian\Tests\problems\ItRunsOnMyPC;

/**
 * @title Problem #4 - It runs on my PC!
 *
 * @description
 * You had a long Thursday at work, writing some not-so-qualified code.
 * You ran a couple of tests on your own workstation, everything worked fine. You go home, but
 * after hours, you get a message from your manager, telling you that our website has shutdown and nothing works anymore.
 * What do you do?
 *
 * @objective Make this work!
 *
 * @note Suggested time to solve: 30 minutes
 *
 * @category PHP extensions, PHP UTF strings, Referenced values
 *
 * @author Hotelian.com LTD; London, UK.
 */
final class ItRunsOnMyPC
{
    private $values = [];

    public function getValues() {
        return $this->values;
    }

    /**
     * @todo Fix: $name='Zürich' does not work!!
     * @todo Fix: Undefined error thrown!
     * @todo Fix: Undefined
     *
     * @param string $name
     *
     * @return string|null
     */
    public function fixMe(string $name): ?string
    {
        if (strlen($name) > 6) {
            return null;
        }
        $this->getValues()['name'] = ucfirst($name);
        return \MongoDB\BSON\fromPHP([$this->getValues()['name']]);
    }

}
